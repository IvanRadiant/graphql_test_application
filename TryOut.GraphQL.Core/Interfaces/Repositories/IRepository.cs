﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TryOut.GraphQL.Core.Entities;

namespace TryOut.GraphQL.Core.Interfaces.Repositories
{
    public interface IRepository<T> where T : BaseEntity
    {
        Task<IEnumerable<T>> GetAll();
        Task<IEnumerable<T>> Get(Func<T, bool> predicate);
        Task<T> Get(int id);
        Task<T> Insert(T entity);
        Task Update(T entity);
        Task Remove(T entity);
        Task<IDictionary<int, T>> GetById(IEnumerable<int> ids, CancellationToken token);
    }
}

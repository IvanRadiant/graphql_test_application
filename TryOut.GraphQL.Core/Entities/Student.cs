﻿using System.Collections.Generic;

namespace TryOut.GraphQL.Core.Entities
{
    public class Student : BaseEntity
    {
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public List<StudentCourse> StudentCourses { get; set; }
        public List<Post> Posts { get; set; }

        public Student()
        {
            StudentCourses = new List<StudentCourse>();
            Posts = new List<Post>();
        }
    }
}

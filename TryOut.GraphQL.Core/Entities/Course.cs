﻿using System.Collections.Generic;

namespace TryOut.GraphQL.Core.Entities
{
    public class Course : BaseEntity
    {
        public string Name { get; set; }
        public List<StudentCourse> StudentCourses { get; set; }

        public Course()
        {
            StudentCourses = new List<StudentCourse>();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TryOut.GraphQL.Core.Entities
{
    public class Post : BaseEntity
    {
        public string Title { get; set; }
        
        public int StudentId { get; set; }
        public Student Student { get; set; }
    }
}

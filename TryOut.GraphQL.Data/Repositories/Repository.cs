﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TryOut.GraphQL.Core.Entities;
using TryOut.GraphQL.Core.Interfaces.Repositories;

namespace TryOut.GraphQL.Data.Repositories
{
    public class Repository<T, TContext> : IRepository<T> where T : BaseEntity
                                                          where TContext : DbContext
    {
        protected readonly TContext context;
        protected DbSet<T> entity;
        protected TaskFactory taskFactory;

        public Repository(TContext context)
        {
            this.context = context;
            entity = context.Set<T>();
            taskFactory = new TaskFactory();
        }

        public async Task<IEnumerable<T>> Get(Func<T, bool> predicate)
        {
            return await taskFactory.StartNew(() => entity.Where(predicate));
        }

        public async Task<T> Get(int id)
        {
            return await entity.FindAsync(id);
        }

        public async Task<IEnumerable<T>> GetAll()
        {
            return await taskFactory.StartNew(() => entity.AsEnumerable<T>());
        }

        public async Task<IDictionary<int, T>> GetById(IEnumerable<int> ids, CancellationToken token)
        {
            return await entity.Where(i => ids.Contains(i.Id)).ToDictionaryAsync(i => i.Id);
        }

        public async Task<T> Insert(T entity)
        {
            await this.entity.AddAsync(entity);
            await context.SaveChangesAsync();

            return entity;
        }

        public async Task Remove(T entity)
        {
            await taskFactory.StartNew(() => this.entity.Remove(entity));
            await context.SaveChangesAsync();
        }

        public async Task Update(T entity)
        {
            await taskFactory.StartNew(() => this.entity.Update(entity));
            await context.SaveChangesAsync();
        }
    }
}

﻿using GraphQL;
using GraphQL.Types;
using GraphQLAPI.Mutations;
using GraphQLAPI.Queries;

namespace GraphQLAPI.GraphQLSchema
{
    public class ApplicationSchema : Schema
    {

        public ApplicationSchema(IDependencyResolver resolver) : base(resolver)
        {
            Query = resolver.Resolve<ApplicationQuery>();
            Mutation = resolver.Resolve<ApplicationMutation>();
        }
    }
}

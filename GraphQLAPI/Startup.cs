﻿using GraphQL;
using GraphQL.DataLoader;
using GraphQL.Http;
using GraphQL.Types;
using GraphQLAPI.GraphQLSchema;
using GraphQLAPI.GraphQLTypes;
using GraphQLAPI.Middlewares;
using GraphQLAPI.Mutations;
using GraphQLAPI.Queries;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TryOut.GraphQL.Core.Entities;
using TryOut.GraphQL.Core.Interfaces.Repositories;
using TryOut.GraphQL.Data;
using TryOut.GraphQL.Data.Repositories;

namespace GraphQLAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddEntityFrameworkSqlServer()
                    .AddDbContext<ApplicationContext>(options => options.UseSqlServer("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=GraphQLDB;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False"));

            services.AddScoped<IRepository<Student>, Repository<Student, ApplicationContext>>();
            services.AddScoped<IRepository<Post>, Repository<Post, ApplicationContext>>();
            services.AddScoped<IRepository<Course>, Repository<Course, ApplicationContext>>();
            services.AddScoped<IRepository<StudentCourse>, Repository<StudentCourse, ApplicationContext>>();

            services.AddScoped<IDependencyResolver>(s => new FuncDependencyResolver(s.GetRequiredService));
            services.AddSingleton<IDocumentWriter, DocumentWriter>();
            services.AddSingleton<IDocumentExecuter, DocumentExecuter>();

            services.AddScoped<ApplicationQuery>();
            services.AddScoped<ISchema, ApplicationSchema>();
            services.AddScoped<ApplicationMutation>();

            services.AddScoped<StudentType>();
            services.AddScoped<StudentInputType>();

            services.AddScoped<PostType>();
            services.AddScoped<PostInputType>();

            services.AddScoped<CourseType>();
            services.AddScoped<CourseInputType>();

            services.AddScoped<StudentCourseType>();
            services.AddScoped<StudentCourseInputType>();

            services.AddSingleton<IDataLoaderContextAccessor, DataLoaderContextAccessor>();
            services.AddSingleton<DataLoaderDocumentListener>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseDefaultFiles();
            app.UseStaticFiles();

            app.UseMiddleware<GraphQLMiddleware>();
        }
    }
}

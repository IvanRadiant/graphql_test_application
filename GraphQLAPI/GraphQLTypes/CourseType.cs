﻿using GraphQL.Types;
using TryOut.GraphQL.Core.Entities;

namespace GraphQLAPI.GraphQLTypes
{
    public class CourseType: ObjectGraphType<Course>
    {
        public CourseType()
        {
            Field(i => i.Id);
            Field(i => i.Name);
        }
    }
}
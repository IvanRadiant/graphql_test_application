﻿using GraphQL.Types;
using TryOut.GraphQL.Core.Entities;
using TryOut.GraphQL.Core.Interfaces.Repositories;

namespace GraphQLAPI.GraphQLTypes
{
    public class StudentCourseType: ObjectGraphType<StudentCourse>
    {
        public StudentCourseType(IRepository<Student> studentRepository, IRepository<Course> courseRepository)
        {
            Field<StudentType, Student>()
                .Name("Student")
                .ResolveAsync(
                    ctx => studentRepository.Get(ctx.Source.StudentId)
                 );

            Field<CourseType, Course>()
                .Name("Course")
                .ResolveAsync(
                    ctx => courseRepository.Get(ctx.Source.CourseId)
                 );
        }
    }
}

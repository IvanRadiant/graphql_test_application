﻿using GraphQL.Types;
using System.Collections.Generic;
using TryOut.GraphQL.Core.Entities;
using TryOut.GraphQL.Core.Interfaces.Repositories;

namespace GraphQLAPI.GraphQLTypes
{
    public class StudentType : ObjectGraphType<Student>
    {
        public StudentType(IRepository<Post> repository)
        {
            Field(i => i.Id);
            Field(i => i.FirstName);
            Field(i => i.Surname);
            Field(i => i.LastName);
            Field(i => i.Age);
            Field<ListGraphType<PostType>, IEnumerable<Post>>()
                .Name("Posts")
                .Resolve(
                    ctx => repository.Get(post => post.StudentId == ctx.Source.Id).GetAwaiter().GetResult()
                );
        }
    }
}

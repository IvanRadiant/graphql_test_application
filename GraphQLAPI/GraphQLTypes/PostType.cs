﻿using GraphQL.DataLoader;
using GraphQL.Types;
using TryOut.GraphQL.Core.Entities;
using TryOut.GraphQL.Core.Interfaces.Repositories;

namespace GraphQLAPI.GraphQLTypes
{
    public class PostType : ObjectGraphType<Post>
    {
        public PostType(IRepository<Student> repository, IDataLoaderContextAccessor accessor)
        {
            Field(i => i.Id);
            Field(i => i.Title);
            Field<StudentType, Student>()
                .Name("Student")
                .ResolveAsync(ctx => {
                    var studentLoader = accessor.Context.GetOrAddBatchLoader<int, Student>("GetById", repository.GetById);
                    return studentLoader.LoadAsync(ctx.Source.StudentId);
                });
        }
    }
}
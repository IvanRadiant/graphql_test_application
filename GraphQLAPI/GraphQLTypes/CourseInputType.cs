﻿using GraphQL.Types;
namespace GraphQLAPI.GraphQLTypes
{
    public class CourseInputType : InputObjectGraphType
    {
        public CourseInputType()
        {
            Name = "CourseInput";
            Field<NonNullGraphType<StringGraphType>>("Name");
        }
    }
}

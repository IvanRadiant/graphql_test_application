﻿using GraphQL.Types;

namespace GraphQLAPI.GraphQLTypes
{
    public class StudentCourseInputType: InputObjectGraphType
    {
        public StudentCourseInputType()
        {
            Name = "StudentCourseInput";
            Field<NonNullGraphType<IntGraphType>>("studentId");
            Field<NonNullGraphType<IntGraphType>>("courseId");
        }
    }
}

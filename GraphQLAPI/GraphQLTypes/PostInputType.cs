﻿using GraphQL.Types;

namespace GraphQLAPI.GraphQLTypes
{
    public class PostInputType : InputObjectGraphType
    {
        public PostInputType()
        {
            Name = "PostInpout";
            Field<NonNullGraphType<StringGraphType>>("Title");
            Field<NonNullGraphType<IntGraphType>>("StudentId");
        }
    }
}

﻿using GraphQL.Types;

namespace GraphQLAPI.GraphQLTypes
{
    public class StudentInputType : InputObjectGraphType
    {
        public StudentInputType()
        {
            Name = "StudentType";
            Field<NonNullGraphType<StringGraphType>>("FirstName");
            Field<NonNullGraphType<StringGraphType>>("Surname");
            Field<NonNullGraphType<StringGraphType>>("LastName");
            Field<NonNullGraphType<IntGraphType>>("Age");
        }
    }
}

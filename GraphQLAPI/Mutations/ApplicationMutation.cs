﻿using GraphQL.Types;
using GraphQLAPI.GraphQLTypes;
using TryOut.GraphQL.Core.Entities;
using TryOut.GraphQL.Core.Interfaces.Repositories;

namespace GraphQLAPI.Mutations
{
    public class ApplicationMutation : ObjectGraphType
    {
        public ApplicationMutation(IRepository<Student> studentRepository, 
                                   IRepository<Post> postRepository, 
                                   IRepository<Course> courseRepository,
                                   IRepository<StudentCourse> studentCourseRepository)
        {
            Field<StudentType>(
                "createStudent",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<StudentInputType>> { Name = "student" }
                ),
                resolve: context =>
                {
                    var item = context.GetArgument<Student>("student");
                    return studentRepository.Insert(item);
                });

            Field<PostType>(
                "createPost",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<PostInputType>> { Name = "post" }
                ),
                resolve: context =>
                {
                    var item = context.GetArgument<Post>("post");
                    return postRepository.Insert(item);
                });

            FieldAsync<CourseType>(
                "createCourse",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<CourseInputType>> { Name = "course" }
                ),
                resolve: async context =>
                {
                    var item = context.GetArgument<Course>("course");
                    return await courseRepository.Insert(item);
                });

            Field<StudentCourseType, StudentCourse>()
                .Name("addStudentCourse")
                .Argument<NonNullGraphType<StudentCourseInputType>>("studentcourse", "studetncourse input")
                .ResolveAsync(ctx =>
                {
                    var studentCourse = ctx.GetArgument<StudentCourse>("studentcourse");
                    return studentCourseRepository.Insert(studentCourse);
                });
        }
    }
}

﻿using GraphQL.Types;
using GraphQLAPI.GraphQLTypes;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TryOut.GraphQL.Core.Entities;
using TryOut.GraphQL.Core.Interfaces.Repositories;

namespace GraphQLAPI.Queries
{
    public class ApplicationQuery : ObjectGraphType
    {
        public ApplicationQuery(IRepository<Student> studentRepository,
                                IRepository<Post> postRepository,
                                IRepository<Course> courseRepository,
                                IRepository<StudentCourse> studentCourseRepository)
        {

            Field<ListGraphType<StudentType>, IEnumerable<Student>>()
                .Name("students")
                .ResolveAsync(ctx => studentRepository.GetAll());

            Field<ListGraphType<PostType>, IEnumerable<Post>>()
                .Name("posts")
                .ResolveAsync(ctx => postRepository.GetAll());

            Field<ListGraphType<CourseType>, IEnumerable<Course>>()
                .Name("courses")
                .ResolveAsync(ctx => courseRepository.GetAll());

            Field<ListGraphType<StudentCourseType>, IEnumerable<StudentCourse>>()
                .Name("studentCourses")
                .ResolveAsync(ctx => studentCourseRepository.GetAll());

            FieldAsync<ListGraphType<StudentCourseType>>(
                    "studentCoursesByCourse",
                    arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "courseId" }),
                    resolve: async context =>
                    {
                        var courseId = context.GetArgument<int>("courseId");
                        return await studentCourseRepository.Get(r => r.CourseId == courseId);
                    }
                );
           

        }
    }
}
